## Dependecies
*react*: 16.4.0

*react-dom*: 16.4.0

*react-router-dom*: 4.2.2

*react-scripts*: 1.1.4

*react-transition-group*: 1.2.1

## How to install
`npm install -g create-react-app`

`create-react-app pki`

`cd pki`

`npm install react-router-dom --save`

`npm install react-transition-group@1.x --save`

`npm start`