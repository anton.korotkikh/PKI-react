import React from 'react'

class CertificateForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
		inp1: '',
		inp2: 2222
    };

    this.handleInputChange = this.handleInputChange.bind(this);
	this.handleSubmit = this.handleSubmit.bind(this);
  }

  
  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  
  handleSubmit(event) {
    alert('Input1: ' + this.state.inp1 + '\nInput2: ' + this.state.inp2);
    event.preventDefault();
  }

  render() {
    return (
		<form onSubmit={this.handleSubmit}>
			<div className="form-row">
				<div className="col-4">
					<label for='inp1id'>Input1</label>
					<input
						name="inp1"
						id='inp1id'
						type="text"
						className="form-control"
						onChange={this.handleInputChange} />
				</div>
			</div>
			<div className="form-row">
				<div className="col-4">
					<label for='inp2id'>Input2</label>
					<input
						name="inp2"
						id='inp2id'
						type="text"
						className="form-control"
						value={this.state.inp2}
						onChange={this.handleInputChange} />
				</div>
			</div>
			<br />
			<div className="form-row">
				<input type="submit" value="Generate" className='btn btn-outline-dark' />
			</div>
      </form>
    );
  }
}
export default CertificateForm