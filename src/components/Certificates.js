import React from 'react'
import { CSSTransitionGroup } from 'react-transition-group'
import CertificateForm from './CertificateForm'
import '../styles/certificatesStyle.css'

const Certificates = () => {
  return (
    <CSSTransitionGroup
      transitionName="aboutTransition"
      transitionAppear={true}
      transitionAppearTimeout={500}
      transitionEnter={false}
      transitionLeave={false}>
	  <h1>Certificates</h1>
	  <CertificateForm />
    </CSSTransitionGroup>
  )
}

export default Certificates
