import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import Domains from './Domains'
import Certificates from './Certificates'

const Content = () => {
  return (
	<div className='container'>
		<Switch>
		  <Route exact path="/" component={Home} />
		  <Route path="/domains" component={Domains} />
		  <Route path="/certificates" component={Certificates} />
		</Switch>
	</div>
  )
}

export default Content
