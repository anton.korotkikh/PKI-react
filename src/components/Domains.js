import React from 'react'
import { CSSTransitionGroup } from 'react-transition-group'
import '../styles/domainsStyle.css'

const Domains = () => {
  return (
    <CSSTransitionGroup
      transitionName="worksTransition"
      transitionAppear={true}
      transitionAppearTimeout={500}
      transitionEnter={false}
      transitionLeave={false}>
	  <h1>Domains</h1>
	</CSSTransitionGroup>
  )
}

export default Domains
