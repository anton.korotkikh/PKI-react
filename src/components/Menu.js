import React from 'react'
import { NavLink } from 'react-router-dom'
import '../styles/menuStyle.css'

const Menu = () => {
  return (
		<nav className='navbar bg-dark'>
			<div className='container'>
				<ul className='nav navbar'>
					<NavLink to="/"><span className="navbar-brand"><img src='/logo.png' alt='Logo'/>Private Key Infrastructure</span></NavLink>
					<li className='nav-item bg-dark'><NavLink to="/domains" activeClassName='active' className='nav-link'>Domains</NavLink></li>
					<li className='nav-item bg-dark'><NavLink to="/certificates" activeClassName='active' className='nav-link'>Certificates</NavLink></li>
				</ul>
			</div>
		</nav>
  )
}

export default Menu
